$(document).ready(function (){

var segundosIniciales;
var caballoi=0;
var caballoj=0;
var registroMovimientos = "Registro movimientos:";
var textarea;
var botonReiniciar;
var botonVerSolucion;

  //Inicia el cronómetro
  function iniciaCronometro() {
    var cadena_tiempo ="Tiempo " + "<br/>" + "00:00";
    segundosIniciales = new Date().getSeconds();
    var cronometro = document.getElementById("cronometro")
    cronometro.innerHTML = cadena_tiempo;
  }


function dentroTablero(x, y){

return ((x>=0) && (x<=7) && (y>=0) && (y<=7));
}
   

function buscaPosicionesCaballo(){

//Habrá como máximo 8 posiciones de caballo

//Se van a buscar las posiciones posibles siguiendo
//el sentido de las agujas de reloj
var mvtos = new Array(1, -2, 2, -1, 2, 1, 1, 2, -1, 2, -2, 1, -2, -1, -1, -2);

var posibles = new Array(null, null, null, null, null, null, null, null, null, null, null, null, null ,null, null, null);

var temp_i;
var temp_j;
	
	insertaLinea("Posibles movimientos del caballo:")
	for (i=0;i<=16;i=i+2)
	{
		temp_i = caballoi + mvtos[i];
		temp_j = caballoj + mvtos[i+1]; 

		if (dentroTablero(temp_i, temp_j)){
			//las anotamos en el array
			posibles[i] = temp_i;
			posibles[i+1] = temp_j;
			//cambiamos de color a dichas celdas
    		var nombre_celda = "c" + temp_i + temp_j;
    		var celda = document.getElementById(nombre_celda);
			insertaLinea("Celda: " + nombre_celda );
    		if (celda != null){
				celda.style.background = 'none';
				celda.style.backgroundColor = "yellow";
			}
		}
	}
} 
function ventanaModal(){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sí"
    }, function(){
  console.log("Ya se ha pulsado el botón de aceptar");
              });
    
    
    swal.fire('Pulse OK para empezar partida'); 
    return;
}

function espera(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}
  
  //actualiza el cronómetro
  function actualizaCronometro() {
    var segundosTranscurridos = new Date().getSeconds() - segundosIniciales;
    var segundosTranscurridosCadena = segundosTranscurridos.toString();
    var cadena_tiempo = "Tiempo <br/>" + segundosTranscurridosCadena;
    document.getElementById("cronometro").innerHTML = cadena_tiempo;
      
  }

 function inicializaCaballo(){

    var nombre_celda = "c" + caballoi + caballoj;
    var celda = document.getElementById(nombre_celda);
    celda.style.backgroundImage = "url('img/caballo.png')";
    celda.style.backgroundRepeat = "no-repeat";
    celda.style.backgroundPosition = "center";
 

	insertaLinea("Posicion inicial caballo " + nombre_celda);
}

function insertaLinea(linea){
	registroMovimientos = registroMovimientos + "\n" + linea;
	//console.log(registroMovimientos);
	if (textarea != null){
		textarea.innerHTML = registroMovimientos;
	}
}
 
function principioPartida() {
	//hacemos primero que se visualice la ventana modoal
    //document.getElementById("#ventanaModal").modal("show");
    //ventanaModal();
	inicializaCaballo();  
    iniciaCronometro();

	textarea = document.getElementById('textarea');
    //Inciamos un timer para que cada segundo actualice el cronómetro
    setInterval(actualizaCronometro, 1000);
	buscaPosicionesCaballo();
    
	botonReiniciar = document.getElementById("botonReiniciar");
	botonVerSolucion = document.getElementById("botonVerSolucion");
  
    botonReiniciar.addEventListener('click', function(e){
			  location.reload();
	});

	
	botonVerSolucion.addEventListener('click', function(e){
  		window.open("img/solucion_caballo.jpeg","solución","width=225,height=225,scrollbars=NO") 
	});

 }


  principioPartida();
    
});

